import React from "react"
import useWindowDimensions from "../hooks/useWindowDimensions"
import PropTypes from "prop-types"
import "../fonts/fonts.css"
import "./contentbox.css"

const ContentBox = ({ children }) => {
  const { width } = useWindowDimensions();
  const DESKTOP_RES = 1280;

  // force Gatsby to re-render so the correct CSS property is applied
  if (typeof window === `undefined`) {
    return <></>
  }

  return (
    <>
      {/* <Header siteTitle={data.site.siteMetadata.title} /> */}
      <div className={`header ${width > DESKTOP_RES ? "" : "mobile"}`}>
        <h1>(Inde)penden</h1>
        <p>
          Pelatihan praktis pengembangan produk, dari{" "}
          <a href="https://www.labtekindie.com" className="white">
            Labtek Indie
          </a>
        </p>
      </div>
      <main className={width > DESKTOP_RES ? "" : "mobile"}>
        {children}
      </main>
    </>
  )
}

ContentBox.propTypes = {
  children: PropTypes.node.isRequired,
}

export default ContentBox
