import React from "react"
import PropTypes from "prop-types"
import { Rnd } from "react-rnd"
import Collapsible from "react-collapsible"
import useWindowDimensions from "../hooks/useWindowDimensions"
import "../fonts/fonts.css"
import "./floatingbox.css"

const rndManagerRef = {
  maxZIndex: "999",
  prevDraggedNode: null,
  prevDraggedNodeZIndex: null,
}

const FloatingBox = ({ children }) => {
  const { width } = useWindowDimensions();
  const DESKTOP_RES = 1280;

  // force Gatsby to re-render so the correct CSS property is applied
  if (typeof window === `undefined`) {
    return <></>
  }

  return (
    <>
      {width > DESKTOP_RES ? (
        <Rnd
          default={{
            width: 360,
            x: 800 * Math.random(),
            y: 300 * Math.random(),
          }}
          enableResizing={{
            top: false,
            right: true,
            bottom: true,
            left: true,
            topRight: true,
            bottomRight: true,
            bottomLeft: false,
            topLeft: false,
          }}
          onDragStart={(e, node) => {
            const manager = rndManagerRef
            if (manager.prevDraggedNode) {
              manager.prevDraggedNode.style.zIndex =
                manager.prevDraggedNodeZIndex
            }
            manager.prevDraggedNode = node.node
            manager.prevDraggedNodeZIndex = manager.prevDraggedNode.style.zIndex
            manager.prevDraggedNode.style.zIndex = manager.maxZIndex
          }}
        >
          <main className="handle">
            <div className="boxContent">
              <Collapsible trigger={children[0]} open="true">
                {children[1]}
              </Collapsible>
            </div>
          </main>
        </Rnd>
      ) : (
        <div className="staticBox">
          <main className="static">
            <div className="boxContent">
              <Collapsible trigger={children[0]} open="true">
                {children[1]}
              </Collapsible>
            </div>
          </main>
        </div>
      )}
    </>
  )
}

FloatingBox.propTypes = {
  children: PropTypes.node.isRequired,
}

export default FloatingBox
