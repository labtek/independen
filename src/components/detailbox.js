import React from "react"
import useWindowDimensions from '../hooks/useWindowDimensions'
import PropTypes from "prop-types"
import "../fonts/fonts.css"
import "./detailbox.css"

const DetailBox = ({ children }) => {
  const { width } = useWindowDimensions();
  const DESKTOP_RES = 1280;

  // force Gatsby to re-render so the correct CSS property is applied
  if (typeof window === `undefined`) {
    return <></>
  }

  return (
    <>
      <div className={`detailBoxContainer ${width > DESKTOP_RES ? "" : "static"}`}>
        {/* <Header siteTitle={data.site.siteMetadata.title} /> */}
        {children}
      </div>
    </>
  )
}

DetailBox.propTypes = {
  children: PropTypes.node.isRequired,
}

export default DetailBox
