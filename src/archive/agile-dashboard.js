import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const AgileDashboardPage = () => (
  <DetailBox>
    <SEO title="Agile Dashboard" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 006 - Agile Dashboard dengan Airtable</h2>
      <p>
        Untuk benar-benar menjadi agile, sebuah tim dalam organisasi dituntut
        tidak hanya bisa menyelesaikan tugasnya, namun, mereka pun harus bisa
        menyampaikan informasi baik secara internal maupun eksternal, dengan
        cara yang transparan dan dapat dipertanggungjawabkan. Selain itu, hasil
        yang telah dicapai sebuah tim pun, harus bisa didokumentasikan dengan
        rapi dan terstruktur, sehingga kemajuan bisa dilihat oleh semua elemen
        organisasi, tidak hanya untuk tim tersebut. Dengan demikian, keberadaan
        sebuah dashboard yang bisa digunakan untuk memantau dan mengukur
        perkembangan sebuah tim, menjadi mutlak dibutuhkan oleh sebuah
        organisasi yang ingin menjadi agile.
      </p>
      <p>
        Dalam kelas kali ini, Saska, co-founder dan principal consultant Labtek
        Indie, akan menunjukkan bagaimana Labtek Indie menggunakan Airtable
        sebagai alat untuk memonitor kerja tim, baik untuk pekerjaan Scrum
        maupun Design Research. Ini dilakukan agar pekerjaan, hasil, serta
        capaian tim bisa dilihat secara transparan dan terdokumentasi dengan
        rapi. Dengan ini, sebuah organisasi bisa memiliki single source of truth
        dalam konteks sebuah pekerjaan.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 15 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Menyusun Product Backlog menggunakan Airtable Agile Dashboard.</li>
        <li>
          Menggunakan UX Nuggets pada Airtable Agile Dashboard untuk
          mendokumentasikan proses User Research.
        </li>
        <li>Simulasi penggunaan Agile Dashboard.</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 10 Mei 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-006-agile-dashboard">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default AgileDashboardPage
