import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const MengkomunikasikanDesainPage = () => (
  <DetailBox>
    <SEO title="Storytelling dalam UX" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 003 - Mengkomunikasikan Desain</h2>
      <p>
        Kamu sudah bekerja keras membuat desain UI yang indah untuk produk yang
        kamu pegang. Kamu sudah berpeluh menghasilkan desain UX yang mulus untuk
        kebaikan pengguna produkmu. Sekarang pertanyaannya, bagaimana
        mengkomunikasikan desain ini, sehingga ia bisa diwujudkan menjadi produk
        yang utuh? Bagaimana menyampaikan niat desainmu, sehingga teman satu tim
        bisa paham dan mengimplementasikan karyamu? <br /> Training kali ini
        akan membahas salah satu aspek penting yang akan dialami seorang
        desainer UI atau UX, yakni mengkomunikasikan desain.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Mendokumentasikan hasil desain</li>
        <li>Seni menanyakan pertanyaan yang tepat</li>
        <li>Menempatkan desain sesuai dengan feedback yang diharapkan</li>
        <li>Berkolaborasi lintas disiplin</li>
        <li>Simulasi</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 19 April 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-003-mengkomunikasikan-desain-qucu">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default MengkomunikasikanDesainPage
