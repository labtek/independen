import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const PickPriorityPage = () => (
  <DetailBox>
    <SEO title="Agile Dashboard" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 009 - Pick Your Priority</h2>
      <p>
        Begitu banyak hal yang harus dilakukan, tapi begitu sedikit waktu yang
        ada. Terkadang (atau bahkan seringkali) rasanya sulit sekali untuk bisa
        benar-benar produktif di pekerjaan, sembari bisa berkontribusi di
        kehidupan keluarga sambil tidak lupa memberikan waktu bagi diri sendiri.
        Lalu apa yang bisa kita lakukan untuk menjawab masalah itu? Bisakah kita
        belajar dari filosofi Agile untuk dipraktekkan di sini?
      </p>
      <p>
        Dalam kelas online ini, Catra Darusman, Product Development di Kumpul
        akan membagikan tips praktis yang bisa kita aplikasikan untuk mengatur
        prioritas hidup, apa saja yang harus dikerjakan dulu dan bagaimana waktu
        bisa dikelola dengan lebih baik. Sehingga kita bisa tetap produktif,
        tetap bisa menjadi anggota keluarga yang baik, namun tak lupa memberikan
        waktu untuk hobi dan pengembangan pribadi.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 13 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Filosofi pengaturan waktu</li>
        <li>Bagaimana mengkategorikan pekerjaan dan prioritas</li>
        <li>Simulasi merencanakan prioritas tugas harian</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 17 Mei 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-009-pick-your-priority">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default PickPriorityPage
