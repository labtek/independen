import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const CraftYourWorkshopPage = () => (
  <DetailBox>
    <SEO title="Craft Your Own Workshop" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 010 - Craft Your Own Workshop</h2>
      <p>
        Menambah pengetahuan dan keterampilan, memiliki peran penting dalam
        pengembangan karir seorang profesional. Salah satu kemampuan yang
        dibutuhkan untuk pengembangan karir profesional adalah Problem Solving
        atau kemampuan memecahkan suatu masalah. Ada banyak cara untuk
        meningkatkan ketrampilan ini, mulai dengan mengkonsumsi materi pelatihan
        baik berupa video atau mengikuti pelatihan secara langsung, membaca
        buku, ataupun dengan menggunakan waktu demi mengasah keterampilan
        pribadi. Namun di luar itu, ternyata ada satu metode lain yang cukup
        membantu kita dalam menaikkan keterampilan problem solving kita, yakni
        dengan mengadakan sendiri workshop untuk kalangan tertentu.
      </p>
      <p>
        Dalam kelas kali ini, Catra Darusman akan membagikan ilmu dan
        pengalamannya dalan mengorganisir workshop, yang terbukti sudah membantu
        dia dalam meningkatkan ketajaman pemecahan masalah serta mengakselerasi
        karirnya. Yak, workshop kali ini cukup unik, karena kita akan mengadakan
        kelas yang mempelajari cara membuat kelas.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 15 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Kelas ini akan membantu kamu mempersiapkan workshop, berbekal pembekalan
        teori, simulasi kecil, serta template yang bisa kamu gunakan untuk acara
        workshop kamu.
      </p>
      <ul>
        <li>Mengapa mengadakan workshop?</li>
        <li>Case Study</li>
        <li>Merencanakan sebuah workshop</li>
        <li>Simulasi melakukan workshop</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Sabtu, 13 Juni 2020, 15.00-16.30</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-010-craft-your-own-workshop">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default CraftYourWorkshopPage
