import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const BasicDesignThinkingPage = () => (
  <DetailBox>
    <SEO title="Agile Dashboard" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 008 - Basic Design Thinking</h2>
      <p>
        Kalau kamu pernah belajar step2 design thinking dan hapal setiap
        langkahnya, tapi kesulitan mengaplikasikannya untuk real project-mu,
        nah, ini dia kelas kecil untukmu. Kamu bisa mengaplikasikan pengetahuan
        ini, baik di proyek sosialmu, proyek ambisiusmu, pekerjaan profesional
        atau bahkan untuk hal-hal yang harus kamu lakukan setiap harinya. Design
        Thinking adalah framework, bukan hanya metode, sehingga seharusnya ia
        bisa mempengaruhimu di tingkat mindset.
      </p>
      <p>
        Dalam kelas online ini, Amanda Mita, SAMS Country Project Manager di
        Labtek Indie, sekaligus praktisi Design Thinking akan menunjukkan
        bagaimana mengaplikasikan ilmu ini di kehidupanmu. Kita akan melompati
        jargon dan teori, dan langsung melihat bagaimana Design Thinking bisa
        dipraktekkan di kehidupanmu. Sehingga kamu bisa benar-benar merasakan
        faedah kelas ini.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 10 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Dasar singkat Design Thinking</li>
        <li>Bagaimana mengaplikasikan Design Thinking</li>
        <li>Simulasi</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 17 Mei 2020, 14.00-15.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-008-basic-design-thinking">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default BasicDesignThinkingPage
