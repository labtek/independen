import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const DesignSystemIntroPage = () => (
  <DetailBox>
    <SEO title="Design System 101" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 012 - Design System 101</h2>
      <p>
        Design System mungkin adalah salah satu konsep yang banyak sekali
        dibahas belakangan ini, baik oleh UI designer, UX designer maupun front
        end developer. Sejak Google meluncurkan Material Design, sebuah design
        system yang digunakan secara massal oleh banyak perusahaan di dunia,
        konsep ini memang menjadi perbincangan hangat. Namun, sebagai seorang
        desainer ataupun developer, hal-hal apa saja sih yang perlu diketahui
        dari sebuah design system? Bagaimana menggunakannya? Adakah kaitannya
        dengan hal yang sebelumnya ada, seperti design guideline ataupun brand
        guideline?
      </p>
      <p>
        Dalam kelas kali ini, Adityo Pratomo, akan mengenalkan topik ini kepada
        kamu, berbekal pengalamannya menggunakan dan membuat design system.
        Kebingungan-kebingungan yang ia alami di awal pengalamannya berkutat
        dengan design system, akan diceritakan di sini, sehingga kamu yang ingin
        mencoba menggunakan design system dalam produk yang sedang dibuat, bisa
        lebih pede dan tidak salah kaprah. Kelas ini bisa menjadi langkah
        pertama yang oke buat untuk kamu yang ingin menggunakan sebuah design
        system, baik desainer maupun developer.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 15 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam kelas ini, kita akan membahas hal-hal praktikal berikut, yang bisa
        jadi titik awal kamu dalam menggunakan sebuah Design System:
      </p>
      <ul>
        <li>Pengenalan Design System</li>
        <li>Design System untuk Developer dan Desainer</li>
        <li>Elemen-elemen Design System</li>
        <li>Studi Kasus</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 21 Juni 2020, 10.00-11.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-012-design-system-101">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default DesignSystemIntroPage
