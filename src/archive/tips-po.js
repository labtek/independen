import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const TipsPoPage = () => (
  <DetailBox>
    <SEO title="Tips Seru untuk Product Owner" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 001 - Tips Seru untuk Product Owner</h2>
      <p>
        Menjadi Product Owner memang pekerjaan yang seru. Banyak sekali tanggung
        jawab yang ia pikul, terlebih lagi, ia pun harus bisa berbicara dengan
        orang yang berbeda yang memegang peranan bervariasi pula. Lalu,
        bagaimanakah seorang Product Owner bisa menjalankan perannya dengan
        baik? Dalam webinar ini, kita akan bersama membahas bagaimana
        meningkatkan kualitas seorang Product Owner, sehingga produk yang ia
        pegang memang mampu benar-benar menghasilkan value.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Berbicara bisnis dari kacamata Product Owner</li>
        <li>Manajemen Product Backlog, User Stories dan Acceptance Criteria</li>
        <li>Berinteraksi dengan Scrum Master dan Scrum Team</li>
        <li>Berinteraksi dengan klien</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>11 April 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-001-tips-seru-untuk-product-owner-ec3">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default TipsPoPage
