import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const TipsProduktifKerjaRemotePage = () => (
  <DetailBox>
    <SEO title="Storytelling dalam UX" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 005 - Tips Produktif Kerja Remote</h2>
      <p>
        Banyak di antara kita yang mungkin baru mulai bekerja remote di masa
        pandemi ini dan mengalami perbedaan yang nyata dengan bekerja di kantor.
        Banyak juga yang mungkin mengalami kesulitan dalam menjaga tempo
        kerjanya sehingga produktivitas menurun.
      </p>
      <p>
        Secara natural, bekerja remote di rumah masing-masing memang berbeda
        jauh dengan bekerja di kantor. Akibatnya, banyak kebiasaan dan cara
        bekerja yang perlu diatur ulang, demi mengakomodir kebiasaan baru ini.
        Dari mulai hal fundamental seperti mengatur waktu, hingga yang berkaitan
        dengan teman kerja lain seperti mengatur tata cara meeting.
      </p>
      <p>
        Training ini akan membahas bagaimana cara-cara yang bisa dilakukan baik
        di tingkat perusahaan maupun pribadi, untuk meningkatkan produktivitas
        saat bekerja remote. Sehingga teman-teman tetap bisa berkontribusi baik
        di kantor maupun di rumah.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 15 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Kerja remote vs kerja di kantor</li>
        <li>Mengatur waktu kerja</li>
        <li>Mengatur tugas</li>
        <li>Berkomunikasi secara Transparan dengan rekan kerja</li>
        <li>Berkolaborasi jarak jauh dengan rekan kerja</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 3 Mei 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-005-tips-produktif-kerja-remote-qug3">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default TipsProduktifKerjaRemotePage
