import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const BelajarDariKegagalanPage = () => (
  <DetailBox>
    <SEO title="Agile Dashboard" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>
        Sel 007 - 4 Pelajaran utama dari kegagalan Labtek Indie di tahun 2018
      </h2>
      <p>
        Banyak yang bilang kalau dalam bisnis itu, kegagalan adalah sesuatu yang
        tidak bisa dihindarkan dan keberhasilan itu adalah bonus. Kegagalan
        adalah sesuatu yang tidak bisa dihindarkan dan pasti dialami seseorang
        yang memutuskan untuk berbisnis. Dua tahun lalu, Labtek Indie berada di
        titik nadirnya akibat berbagai kesalahan baik tingkat operasional maupun
        strategis. Beruntung, kami bisa bangkit, selamat dan menceritakan
        pengalaman ini ke teman-teman.
      </p>
      <p>
        Dalam kelas online istimewa ini, Saska, principal consultant sekaligus
        co-founder Labtek Indie akan bercerita kesalahan apa yang kami lakukan
        di tahun itu sehingga kami terjebak di posisi yang sangat sulit. Cerita
        ini kami bagikan, terutama kepada teman-teman yang memiliki aspirasi
        atau bahkan sudah memiliki usaha sendiri, sehingga teman-teman bisa
        belajar dari kesalahan kami.
      </p>
      <p>
        Demi kelancaran, kelas ini terbatas hanya untuk maksimal 10 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam kelas online ini, kita akan bersama melakukan kegiatan-kegiatan berikut:
      </p>
      <ul>
        <li>Cerita kegagalan Labtek Indie</li>
        <li>4 pelajaran utama</li>
        <li>Diskusi bebas</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Sabtu, 16 Mei 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-007-belajar-dari-kegagalan">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default BelajarDariKegagalanPage
