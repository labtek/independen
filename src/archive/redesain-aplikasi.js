import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const RedesainAplikasiPage = () => (
  <DetailBox>
    <SEO title="Storytelling dalam UX" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 006 - Redesain Aplikasi</h2>
      <p>
        Me-redesain ulang aplikasi adalah hal yang akan dilakukan oleh seorang
        UI atau UX desainer di karir mereka. Kegiatan ini bisa jadi dilakukan
        baik di tempat kerja, maupun di masa senggang saat ingin memperkaya
        portfolio pribadi. Kegiatan ini memang punya nilai tinggi, karena bisa
        jadi suatu aplikasi yang sudah ada perlu diatur ulang alur atau tampilan
        visualnya, baik untuk meningkatkan kegunaannya, memuluskan alur saat
        pengguna melakukan task tertentu, ataupun untuk menampilkan tampilan
        yang lebih segar. Sebagai latihan pribadi pun, kegiatan ini sangat seru,
        karena kita sebagai desainer bisa fokus pada merancang aplikasi dan
        sedikit mengurangi porsi seperti memikirkan konsep produk.
      </p>
      <p>
        Kelas kali ini akan lebih istimewa, karena kita akan bersama melakukan
        redesain di Figma, untuk aplikasi yang sudah disiapkan sebelumnya. Kita
        akan sedikit mendengarkan paparan teori, dan lebih banyak melakukan
        praktik langsung. Jadi, jika kamu punya ide aplikasi untuk dirancang
        ulang, jangan takut untuk mempersiapkan filenya sebelum kelas ini
        dimulai.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 15 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Analisa awal aplikasi</li>
        <li>Merencanakan redesain</li>
        <li>Praktek redesain</li>
        <li>Diskusi bersama</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 10 Mei 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-006-redesain-aplikasi-jmu">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default RedesainAplikasiPage
