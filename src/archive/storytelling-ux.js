import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const StorytellingUxPage = () => (
  <DetailBox>
    <SEO title="Storytelling dalam UX" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 002 - Storytelling dalam UX</h2>
      <p>
        Setiap produk harus punya ceritanya masing-masing. Pertanyaannya,
        sebagai seorang desainer UX, sejauh manakah kamu mampu menyampaikan
        narasi ini, sehingga pengguna produk kamu bisa merasa sebagai pahlawan
        di ceritanya? Dalam training ini, kita akan bersama membahas bagaimana
        menempatkan aspek storytelling dalam produk, sehingga pengguna akan
        semakin betah menggunakannya.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Bagaimana cara bercerita</li>
        <li>Membungkus fungsi dengan cerita</li>
        <li>Elemen produk yang membantu menyampaikan cerita</li>
        <li>Studi kasus</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>12 April 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-002-storytelling-dalam-ux-ec4">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default StorytellingUxPage
