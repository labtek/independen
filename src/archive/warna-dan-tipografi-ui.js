import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const WarnaDanTipografiPage = () => (
  <DetailBox>
    <SEO title="Warna dan Tipografi dalam UI" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 011 - Warna dan Tipografi dalam UI</h2>
      <p>
        Warna dan Tipografi (susunan serta penggunaan teks) adalah komponen
        penting yang mendefinisikan UI (User Interface) dalam sebuah produk.
        Penggunaan keduanya secara tepat akan membantu memberikan struktur di
        dalam sebuah UI yang berujung membantu pengguna sebuah produk digital
        untuk mendapatkan informasi yang ia inginkan, sekaligus melakukan sebuah
        pekerjaan demi mendapatkan hal yang ia butuhkan.
      </p>
      <p>
        Dalam kelas kali ini, Adityo Pratomo, akan membahas bagaimana kedua
        elemen ini bisa digunakan untuk membuat UI yang jelas dan tetap
        bergantung pada brand guideline, untuk menjamin kesinambungan antara
        imej perusahan dengan produk yang dibuatnya. Selain itu, kita juga akan
        melihat bagaimana tips praktis penerapan keduanya dalam menyusun sebuah
        UI.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 15 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam kelas ini, kita akan membahas hal-hal praktikal berikut, yang bisa
        kamu coba juga dalam pekerjaan kamu:
      </p>
      <ul>
        <li>Penggunaan Warna dalam UI</li>
        <li>Tipografi dalam UI</li>
        <li>Studi Kasus</li>
        <li>Tips dan simulasi singkat</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 14 Juni 2020, 10.00-11.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-011-warna-dan-tipografi-dalam-ui">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default WarnaDanTipografiPage
