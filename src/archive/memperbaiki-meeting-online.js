import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const MemperbaikiMeetingOnlinemuPage = () => (
  <DetailBox>
    <SEO title="Storytelling dalam UX" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 004 - Memperbaiki Online Meetingmu</h2>
      <p>
        Selama sebulan ini, banyak di antara kita yang bekerja dari rumah. Salah
        satu hal sulit yang dihadapi para pekerja yang mendadak bekerja jarak
        jauh ini adalah tumpukan meeting online yang harus dihadapi setiap
        harinya. Meeting online begitu mudah dilakukan, cukup nyalakan laptop,
        hubungkan dengan aplikasi conference call dan boom, meeting pun
        berjalan.
      </p>
      <p>
        Pertanyaannya, apakah meeting ini benar-benar membantu para pekerja,
        ataukah hanya menambah beban? Jika begitu, bagaimanakah kita bisa
        bersama menjadikan meeting online sebagai sesuatu yang produktif dan
        bermanfaat?
      </p>
      <p>
        Training ini akan membahas bagaimana cara-cara yang bisa dilakukan baik
        di tingkat perusahaan maupun pribadi, untuk memperbaiki cara menjalankan
        meeting online. Jika kita harus berada dalam situasi isolasi ini dalam
        waktu lama, tak ada salahnya memperbaiki cara kita dalam bekerja jarak
        jauh.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 15 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam training online ini, kita akan bersama membahas poin-poin berikut:
      </p>
      <ul>
        <li>Filosofi meeting</li>
        <li>Mempersiapkan meeting online</li>
        <li>Menjalankan meeting online dengan efisien</li>
        <li>Melakukan follow up meeting</li>
        <li>Simulasi</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 26 April 2020, 16.00-17.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-004-memperbaiki-online-meetingmu-q09">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default MemperbaikiMeetingOnlinemuPage
