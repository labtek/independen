import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import FloatingBox from "../components/floatingbox"
import ContentBox from "../components/contentbox"

const IndexPage = () => (
  <ContentBox>
    <SEO title="Home" />
    <FloatingBox>
      <header>Tentang (Inde)penden</header>
      <div>
        <p>
          <strong>(Inde)penden</strong> adalah sub-brand dari{" "}
          <strong>Labtek Indie</strong> yang fokus pada pemberian pelatihan
          praktikal kepada publik. Kami memberikan materi-materi seputar
          pengembangan produk digital, berdasarkan pengalaman kami melakukan
          pengembangan produk-produk untuk berbagai klien, selama lebih dari 7
          tahun.
        </p>
      </div>
    </FloatingBox>
    <FloatingBox>
      <header>Tentang Sel</header>
      <div>
        <p>
          <strong>Sel</strong> adalah kelas online dari{" "}
          <strong>(Inde)penden</strong> yang mempraktekkan pembelajaran secara
          asinkronus. Setiap kelas akan berisi gabungan dari:
        </p>
        <ul>
          <li>penyampaian materi selama 1 jam dalam online training</li>
          <li>praktek mandiri di luar webinar</li>
          <li>diskusi via online chat.</li>
        </ul>
        <p>
          Kelas ini kecil, dan hanya mengakomodir maksimal{" "}
          <strong>10 orang</strong>, sehingga penyampaian materi bisa lebih
          efisien.
        </p>
      </div>
    </FloatingBox>
    <FloatingBox>
      <header>Sel 013 - UX Nuggets</header>
      <div>
        <p>
          Jika kamu pernah atau sering melakukan riset UX, pasti kamu pernah
          merasakan betapa trickynya mendokumentasikan temuan riset. Apakah
          cukup dengan laporan saja, atau adakah cara/metode lain yang bisa
          membuat temuan UX ini bisa lebih dimanfaatkan di fase pengembangan
          produk selanjutnya? Saska akan memperkenalkan UX Nuggets, sebuah cara
          mendokumentasikan riset UX, yang bisa menjawab
          permasalahan-permasalahan tersebut.
        </p>
        <br />
        <Link to="/ux-nuggets">Detail Training</Link>
      </div>
    </FloatingBox>
  </ContentBox>
)

export default IndexPage
