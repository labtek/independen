import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import DetailBox from "../components/detailbox"

const UXNuggetsPage = () => (
  <DetailBox>
    <SEO title="UX Nuggets" />
    <div className="detailBoxTitleContainer">
      <div className="detailBoxTitle">
        <Link to="/">
          <h1>(Inde)penden</h1>
        </Link>
      </div>
    </div>
    <div className="detailBoxFull">
      <h2>Sel 013 - Dokumentasi Riset UX dengan UX Nuggets</h2>
      <p>
        Kamu telah melakukan riset UX kualitatif, baik lewat wawancara, user
        testing ataupun dengan metode lainnya. Kini tiba saatnya kamu
        menganalisa temuan-temuan dari riset ini, untuk mendapatkan sebuah
        insight. Namun, pertanyaannya, bagaimana kita bisa mengolah data yang
        sangat bervariasi ini? Adakah tool atau metode yang bisa membantu kita
        mendokumentasikan temuan, sehingga mudah untuk dianalisa dan dijadikan
        acuan di masa depan?
      </p>
      <p>
        Dalam kelas kali ini, Saska, principal consultant di Labtek Indie, akan
        memperkenalkan sebuah metode bernama UX Nugget, salah satu senjata
        andalan Labtek Indie dalam mendokumentasikan riset kualitatif. Metode
        ini sungguh menarik, karena ia tidak hanya dibuat untuk mencatat temuan
        riset, tapi juga dilakukan dalam format yang sedemikian rupa, sehingga
        memudahkan dalam perumusan insight, pembuatan laporan hingga referensi
        pada saat implementasi pengembangan produk. Tercatat, sudah nyaris 2
        tahun Labtek Indie menggunakan metode ini dan banyak project yang
        pekerjaannya terbantu dengan menggunakan UX Nuggets ini.
      </p>
      <p>
        Demi kelancaran, training ini terbatas hanya untuk maksimal 10 orang.
      </p>
    </div>
    <div className="detailBoxFull">
      <h3>Materi</h3>
      <p>
        Dalam kelas ini, kita akan membahas hal-hal praktikal berikut, yang bisa
        jadi titik awal kamu dalam menggunakan UX Nuggets:
      </p>
      <ul>
        <li>Masalah dalam mendokumentasikan riset</li>
        <li>Mengenal format UX Nuggets</li>
        <li>Mengimplementasikan UX Nuggets di Airtable</li>
        <li>Menggunakan UX Nuggets untuk mencari insight</li>
        <li>Case Study</li>
      </ul>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Format Training</h3>
        <p>Online Training</p>
      </div>
      <div className="item">
        <h3>Tanggal Pelaksanaan</h3>
        <p>Minggu, 28 Juni 2020, 14.00-15.00</p>
      </div>
    </div>
    <div className="halfContainer">
      <div className="item">
        <h3>Biaya Pendaftaran</h3>
        <p>Rp 100.000</p>
      </div>
      <div className="item cta">
        <a href="https://www.loket.com/event/sel-013-ux-nuggets">
          Daftar Sekarang
        </a>
      </div>
    </div>
  </DetailBox>
)

export default UXNuggetsPage
